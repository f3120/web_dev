import uvicorn
from setting_web import app
from routers.users.manager_routers import router as RouterUsers

app.include_router(RouterUsers)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
