import React, { useState, useEffect } from "react";
import InputField from "../../components/InputField/InputField";
import {
  changeName,
  logout,
} from "../../components/submitFunctions/submitFunctions";
import "./MainPage.css";

const MainPage = ({ removeCookie }) => {
  const [userData, setUserData] = useState({ userName: "" });
  return (
    <div className="main">
      <InputField
        fieldName={"userName"}
        value={userData}
        setValue={setUserData}
      ></InputField>
      <div
        className="change_btn btn"
        onClick={() => {
          changeName(userData.userName);
        }}
      >
        Сменить имя
      </div>
      <div
        className="exit_btn btn"
        onClick={() => {
          removeCookie();
          logout();
        }}
      >
        Выйти
      </div>
    </div>
  );
};

export default MainPage;
