import React, { useState, useEffect, useRef } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import InputField from "../../components/InputField/InputField";
import { login } from "../../components/submitFunctions/submitFunctions";
import "./Authorization.css";

function Authorization({
  token,
  handleCookies,
  setTokenTimeOut,
  tokenTimeOut,
}) {
  let navigate = useNavigate();
  let location = useLocation();
  const [regState, setRegState] = useState(false);

  const ref = useRef();
  const refBtn = useRef();

  useEffect(() => {
    if (token) {
      navigate("/");
    }
  });

  useEffect(() => {
    document.addEventListener("keydown", (e) => {
      console.log("e", e);
      const event = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
      });
      location?.pathname === "/auth" &&
        e.code === "Enter" &&
        refBtn.current.dispatchEvent(event);
    });
  }, []);

  const [userData, setUserData] = useState({
    login: "",
    password: "",
    userName: "",
  });
  const [errAuth, setErrAuth] = useState(false);

  useEffect(() => {
    if (errAuth) {
      setErrAuth(false);
    }
  }, [userData, regState]);

  return (
    <div className="author" ref={ref}>
      <div className="author__wrapp">
        {tokenTimeOut ? (
          <div className="author__time-out">Истекло время сессии</div>
        ) : null}
        <div className="author__text">{regState ? "Регистрация" : "Вход"}</div>
        <div className="author__login">
          <InputField
            fieldName={"login"}
            style={{ width: "100%" }}
            value={userData}
            setValue={setUserData}
            error={errAuth}
          ></InputField>
          <InputField
            fieldName={"password"}
            style={{ width: "100%" }}
            value={userData}
            setValue={setUserData}
            error={errAuth}
          ></InputField>
          <div
            className="author__register"
            style={regState ? null : { maxHeight: 0 }}
          >
            <InputField
              fieldName={"userName"}
              style={{ width: "100%" }}
              value={userData}
              setValue={setUserData}
            ></InputField>
          </div>
        </div>
        <div className="author__error" style={{ height: errAuth ? "" : 0 }}>
          {errAuth ? "Неверный логин или пароль" : ""}
        </div>
        <div
          className="author__btn"
          ref={refBtn}
          onClick={async () => {
            let loginResp = await login(userData, regState);
            console.log(loginResp);
            if (loginResp?.data) {
              handleCookies(loginResp?.data?.access_token);
              navigate("/");
              setTokenTimeOut(false);
            } else {
              setErrAuth(true);
            }
          }}
        >
          {regState ? "Зарегистрироваться" : "Войти"}
        </div>
        <div
          className="author__toggle"
          onClick={() =>
            setRegState((regState) => {
              return regState ? false : true;
            })
          }
        >
          {regState ? "Вход" : "Регистрация"}
        </div>
      </div>
    </div>
  );
}

export default Authorization;
