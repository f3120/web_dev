import { useClock } from "../../hooks/clock.hook";
import { Link, useLocation, useNavigate } from "react-router-dom";
import logo from "../../media/logo.svg";
import user from "../../media/UserPic.png";
import "./Header.css";

const Header = ({ mobile, token }) => {
  let { date, time } = useClock();
  let location = useLocation();
  return (
    <div
      className="header"
      style={
        token && location?.pathname !== "/user"
          ? {}
          : { justifyContent: "center" }
      }
    >
      <div className="container">
        {mobile ? (
          <div className="header__calendar" style={{ height: 0 }}></div>
        ) : (
          <div className="header__today">
            <div className="header__date">{date}</div>
            <div className="header__time">{time}</div>
          </div>
        )}

        <Link className="header__logo" to="/">
          <img src={logo} />
        </Link>

        <div className="header__menu" style={{ height: 0 }}>
          <Link
            className="header__user"
            to="/user"
            style={{ textDecoration: "none" }}
          >
            <div className="header__user_text">Мой Олег</div>
            <img src={user} />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Header;
