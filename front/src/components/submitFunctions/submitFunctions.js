import { getCookie } from "../tools/tools";
import { urlUser } from "../../config/config";
import axios from "axios";

export const login = async (userData, regState) => {
  const apiUrl = urlUser + `/users/${regState ? "register" : "login"}`;
  const data = regState
    ? {
        name: userData?.userName,
        login: userData?.login,
        password: userData?.password,
      }
    : {
        login: userData?.login,
        password: userData?.password,
      };
  let access;
  await axios.post(apiUrl, data).then(
    (resp) => {
      console.log("tocken exist");
      //setCookie(resp.data.access_tocken)
      access = resp;
    },
    (err) => {
      console.log("tocken fall");
      access = false;
    }
  );
  return access;
};

export const logout = () => {
  const apiUrl = urlUser + `/users/logout`;
  let cookie = getCookie();
  axios
    .delete(apiUrl, {
      headers: {
        Authorization: `Bearer ${cookie?.token}`,
      },
    })
    .then((resp) => {
      console.log("logout", document?.cookie.split("=")[1]);
    });
};

export const changeName = (newName) => {
  const apiUrl = urlUser + `/users/change_name?new_name=${newName}`;
  let cookie = getCookie();
  axios
    .get(apiUrl, {
      headers: {
        Authorization: `Bearer ${cookie?.token}`,
      },
    })
    .then((resp) => {
      console.log("logout", document?.cookie.split("=")[1]);
    });
};
